The simple-pom-analyzer
=======================

Analyzing parent-child relationships in a maven multi-module projects.

**Input:** the root of the multi-module project to be analayzed.

**Output:** a report HTML containing for each project under the provided root directory for each POM 
the project coordinates (groupId, artifactId, versionId) and the referenced parent coordinates.

In addition the equality of the versionId of the referenced parent compared with the versionId defined 
in the local parent project. The differences are reported in the output html.

Example usage
-------------

You can call it in the following way:

`
$ java -jar target/simple-pom-analyzer-0.1.0-SNAPSHOT-standalone.jar  /home/pirosa/development/maven/ > report.html
`

Please check the 
[example output] (http://htmlpreview.github.com/?https://github.com/pirosa/simple-pom-analyzer/blob/master/doc/report.html).
