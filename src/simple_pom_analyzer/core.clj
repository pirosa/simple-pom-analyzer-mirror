(ns simple-pom-analyzer.core
  (:gen-class)
  (:require [hiccup.core] 
            [clojure.xml] 
            [clojure.zip] 
            [clojure.data.zip.xml :as zipxml]))

(defn getParentSecCoordinates 
  [parent-sec-zipper]
      {:parentArtifactId (zipxml/xml1-> parent-sec-zipper :artifactId zipxml/text)
       :parentGroupId (zipxml/xml1-> parent-sec-zipper :groupId zipxml/text)
       :parentVersion (zipxml/xml1-> parent-sec-zipper :version zipxml/text)
       :parentRelativePath 
       (if-let [relative-path (zipxml/xml1-> parent-sec-zipper :relativePath zipxml/text)] relative-path "..")})

(defn getCurrPrjCoordinates 
  [whole-pom-zipper parent-pom-coordinates]
    {:artifactId (zipxml/xml1-> whole-pom-zipper :artifactId zipxml/text)
     :groupId ; if curr-group-id does not exist then inherited from the parent 
     (if-let
       [curr-group-id (zipxml/xml1-> whole-pom-zipper :groupId zipxml/text)] 
       curr-group-id 
       (:parentGroupId parent-pom-coordinates))
     :version ; if curr ver does not exist then inherited from the parent 
     (if-let
       [curr-version (zipxml/xml1-> whole-pom-zipper :version zipxml/text)] 
       curr-version 
       (:parentVersion parent-pom-coordinates))})

(defn getProjectData 
   "The 'xml-doc' is parsed pom.xml  
    The 'transformed-prj-data' is a map containing the following fields:
    - :artifactId
    - :version
    - :parentArtifactId
    - :parentVersion.
    In this function 
   " 
   
  [xml-doc]
  (let [whole-pom-zipper (clojure.zip/xml-zip xml-doc)
        parent-sec-zipper (zipxml/xml1-> whole-pom-zipper :parent)] 
      (if (nil? parent-sec-zipper)
        (getCurrPrjCoordinates whole-pom-zipper {})
        (let [parent-pom-coordinates 
              (getParentSecCoordinates parent-sec-zipper)] 
          (merge parent-pom-coordinates
                 (getCurrPrjCoordinates whole-pom-zipper parent-pom-coordinates))))))

(defn processPom [pom-file]
  (let 
    [xml-doc 
     (clojure.xml/parse pom-file)] 
    (let [pomFullPath (.getPath pom-file)] 
      (assoc (getProjectData xml-doc) :pomFullPath pomFullPath ))))

(defn createPomDescList [startdir]
  (for 
    [x 
     (file-seq 
       (java.io.File. startdir)) 
     :when 
     (= "pom.xml" 
        (.getName x))] 
    (processPom  x)))

(defn generateArtifactToPrjDescMap [pomDescList ]
  (reduce (fn [myMap element] (assoc myMap (:artifactId element) element))  {} pomDescList))

(defn getVersion [airtifactToPrjMap airtifactId]
    (get (get airtifactToPrjMap airtifactId {}) :version "none"))

(defn generateTableRows [airtifactToPrjMap] 
  (reduce 
    (fn [myVec prjDescPair] 
      (conj myVec 
            (let [mval (val prjDescPair)] 
            [:tr 
             [:td 
              (:pomFullPath mval)] 
             [:td 
              (str (:groupId mval) ":" (:artifactId mval))] 
             [:td 
              (:version mval)] 
             [:td 
              (str (:parentGroupId mval) ":" (:parentArtifactId mval))]
             (if (= (:parentVersion mval) 
                    (getVersion airtifactToPrjMap (:parentArtifactId mval))) 
               [:td {:bgcolor "#ADFF2F"} (:parentVersion mval)] 
               [:td {:bgcolor "#FA8072"} 
                    (str (:parentVersion mval) " (expected: " (getVersion airtifactToPrjMap (:parentArtifactId mval)) ")")])
             [:td (:parentRelativePath mval)]]))) 
    [] 
    airtifactToPrjMap))

(defn generatePomReport [pomDescList ]
  (let [airtifactToPrjMap  (generateArtifactToPrjDescMap pomDescList)] 
;    (getVersion airtifactToPrjMap "parent")))
    (hiccup.core/html 
      [:html
       [:body
        (reduce (fn [x y] 
                  (conj x y)) 
                [:table {:border 1} 
                 [:tr 
                  [:th {:colspan 3} "Current"] 
                  [:th {:colspan 3} "Parent"]
                  ]
                 [:tr 
                  [:th "Path"] 
                  [:th "Coordinate"] 
                  [:th "Version"]  
                  [:th "Coordinate"] 
                  [:th "Version"]
                  [:th "Relative path"]
                  ]] 
                (generateTableRows airtifactToPrjMap))]])))

(generatePomReport 
  (createPomDescList "/home/pirosa/development/maven/grandparent"))

(defn -main
  [& args]
  (println (generatePomReport (createPomDescList (first args)))))
