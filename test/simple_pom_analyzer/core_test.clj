(ns simple-pom-analyzer.core-test
  (:use clojure.test
        clojure.inspector
        simple-pom-analyzer.core))

(def parsed-test-pom
  (clojure.xml/parse 
    (.getFile 
      (clojure.java.io/resource "test_pom.xml"))))

;(clojure.inspector/inspect-tree parsed-test-pom)
;(Thread/sleep 600000)

(deftest getProjectData-test
  (testing "Testing getProjectData"
    (is 
      (= 
        (getProjectData parsed-test-pom) 
        {:version "1.1", :groupId "org.mvnlearn.test1", :artifactId "parent", :parentArtifactId "grandparent", :parentGroupId "org.mvnlearn.test1", :parentVersion "1.0", :parentRelativePath ".."}))))

